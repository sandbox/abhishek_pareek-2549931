<?php
/**
 * @file
 * Contains menu callback of required-field-list page.
 */

/**
 * Callback of "Empty Required Fields" page.
 */
function required_on_publish_list_required() {
  $form_array = drupal_get_form('required_on_publish_node_list_form');
  $output = drupal_render($form_array);

  $type = variable_get('required_on_publish_node_type');
  $output .= required_on_publish_list_required_content($type);

  return $output;
}

/**
 * Filter form.
 */
function required_on_publish_node_list_form($form, &$form_state) {
  $options = array();
  $node_types = node_type_get_types();
  $options['all'] = t('-- All --');
  foreach ($node_types as $node_detail) {
    $options[$node_detail->type] = $node_detail->name;
  }
  $form = array();
  $form['#prefix'] = '<div class="required-on-publish-filter-form">';
  $form['#suffix'] = '</div>';
  $form['node_list'] = array(
    '#type' => 'select',
    '#title' => t('Node Type'),
    '#weight' => 1,
    '#options' => $options,
    '#default_value' => variable_get('required_on_publish_node_type', 'all'),
    '#description' => t('Filter the list by selecting appropriate node type'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Filter'),
    '#weight' => 2,
  );

  return $form;
}

/**
 * Filter form submit handler.
 */
function required_on_publish_node_list_form_submit($form, &$form_state) {
  $type = $form_state['values']['node_list'];
  variable_set('required_on_publish_node_type', $type);
}

/**
 * Generate table information.
 */
function required_on_publish_list_required_content($type = NULL) {
  $header = array(t('Node ID'), t('Title'), t('Empty Required Fields'));
  $rows = array();

  // Get all nodes.
  $query = db_select('node', 'n', array());
  $query->fields('n', array('title'));
  $query->fields('n', array('nid'));
  $query->fields('n', array('type'));
  $query->condition('status', 0, '=');
  if (isset($type) && $type != 'all') {
    $query->condition('type', $type, '=');
  }
  $query = $query->extend('PagerDefault')->limit(10);
  $result = $query->execute()->fetchall();
  if (empty($result)) {
    return t('Nothing to show, seems like every "required on publish" field is filled.');
  }

  // Parse all nodes and check for any left required field.
  foreach ($result as $node) {

    $empty_req_fields = array();
    $title = $node->title;
    $node_content = node_load($node->nid);
    $field_ins = field_info_instances("node", $node->type);

    foreach ($field_ins as $field_ins_val) {
      $field_value = field_get_items('node', $node_content, $field_ins_val['field_name']);
      if (empty($field_value) && isset($field_ins_val['required_on_publish'])) {
        $empty_req_fields[] = $field_ins_val['label'];
      }
    }
    if (!empty($empty_req_fields)) {
      $rows[] = array($node->nid,
        l(t('Edit: @title', array('@title' => $title)), 'node/' . $node->nid . '/edit'),
        implode(', ', $empty_req_fields),
      );
    }
  }

  $output = theme('table', array('header' => $header, 'rows' => $rows));
  $output .= theme('pager');

  return $output;
}
