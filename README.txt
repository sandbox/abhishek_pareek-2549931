INTRODUCTION
------------

Required On Publish will let you save any node in unpublished state even if
there are some required fields left unfilled. Those required fields will act
only when node is saved in published state, i.e. if some required fields are
still left empty while publishing the node then it will not allow until
all the required fields are non-empty.

* For a full description of the module, visit the projectred page:
  https://www.drupal.org/sandbox/abhishek_pareek/2549931

INSTALLATION
------------

* Install as you would normally install a contributed Drupal module.

Enable Required On Publish module which will provide a required on publish
option on field settings form.

See: https://drupal.org/documentation/install/modules-themes/modules-7
for further information.

CONFIGURATION
-------------

Required On Published do not require any configuration, it works right out
of the box, you just have to set which field is required on the field's
setting page, i.e. mark the checkbox [x] Required on Publish instead of []
required if you want any field to be required, instead it has an admin page
which lets you watch which field is set 'required_on_publish' and is empty.


MAINTAINERS
-----------

Current maintainers:
  * Abhishek Pareek (abhishek_pareek) - https://www.drupal.org/user/3231365

This project has been sponsored by:
  * Innoraft Solutions - https://www.drupal.org/node/2145877
